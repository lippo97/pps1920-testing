package testLab

import org.scalatest.{FlatSpec, Matchers}

class CatalogTest extends FlatSpec with Matchers {

  "An empty catalog products" should "have size 0" in {
    new BasicCatalog(Map()).products should have size 0
  }

  it should "throw an exception for any product" in {
    an [NoSuchElementException] should be thrownBy new BasicCatalog(Map()).priceFor(Product("banana"))
  }

  val items = Map(
    (Product("banana"), Price(2.50)),
    (Product("apple"), Price(2)),
    (Product("pineapple"), Price(1)),
    (Product("milk"), Price(10)),
  )

  "A non-empty catalog" should "find prices for existing products" in {
    new BasicCatalog(items) priceFor Product("banana") shouldBe Price(2.50)
  }

  it should "find prices for existing products and multiply by quantity" in {
    new BasicCatalog(items) priceFor(Product("banana"), 2) shouldBe Price(5)
  }

  it should "throw an exception when looking for non-existing products" in {
    an [NoSuchElementException] should be thrownBy new BasicCatalog(items).priceFor(Product("sugar"))
  }
}
