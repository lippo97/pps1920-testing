package testLab

import org.scalatest.{FunSuite, Matchers}

class WarehouseTest extends FunSuite with Matchers {
  test("Get on an non-stored product should return an empty item") {
    new BasicWarehouse().get(Product("banana"), 1) shouldBe(Product("banana"), 0)
  }

  test("Get on an available Product should return it") {
    val warehouse: Warehouse = new BasicWarehouse
    warehouse.supply(Product("banana"), 10)
    warehouse.get(Product("banana"), 3) shouldBe (Product("banana"), 3)
  }
  test("Get for too many Product should return all of the items") {
    val warehouse: Warehouse = new BasicWarehouse
    warehouse.supply(Product("banana"), 10)
    warehouse.get(Product("banana"), 100) shouldBe (Product("banana"), 10)
  }

//  describe("An empty Warehouse") {
//    describe("when a Product is requested") {
//      it("should return an empty Product") {
//        new BasicWarehouse().get(Product("banana"), 1) shouldBe(Product("banana"), 0)
//      }
//    }
//    describe("when a Product is ")
//  }
}
