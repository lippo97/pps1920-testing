package testLab

import org.junit.runner.RunWith
import org.scalacheck.{Arbitrary, Gen, Prop, Properties}
import org.scalacheck.Prop.{exists, forAll}
import testLecture.code.ScalaCheckJUnitRunner

@RunWith(classOf[ScalaCheckJUnitRunner])
class TestOnLists extends Properties("Numbers") {
  property("List concat is associative") = forAll { (xs: Seq[Int], xy: Seq[Int], xz: Seq[Int]) =>
    (xs ++ xy) ++ xz == xs ++ (xy ++ xz)
  }
  property("List concat has empty list as identity") = forAll { (xs: Seq[Int]) =>
    xs ++ Seq() == Seq() ++ xs
  }
  property("List map identity produces the same list") = forAll { (xs: Seq[Int]) =>
    (xs map identity) == xs
  }
  property("List map function are composable") = forAll{ (xs: Seq[Int], f: Function1[Int, Int], g: Function1[Int, Int]) =>
    xs.map(f `compose` g) == xs.map(g).map(f)
  }
}

@RunWith(classOf[ScalaCheckJUnitRunner])
class TestOnPalindromes extends Properties("Seqs") {
  val genCore: Gen[List[Int]] = Gen.oneOf(
    Gen.const(List()),
    Gen.choose(0, 9).map(x => List(x))
  )
  val genPair: Gen[List[Int]] = for {
    n <- Gen.choose(0, 9)
    core <- genPalindrome
  } yield n +: core :+ n
  def genPalindrome: Gen[List[Int]] = Gen.oneOf(genCore, genPair)
  property("Palindromes") = forAll (genPair) { (a: Seq[Int]) =>
    println(a)
    a == a.reverse
  }
}

