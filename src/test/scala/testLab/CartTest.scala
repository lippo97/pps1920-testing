package testLab

import org.scalatest.{FunSpec, FunSuite, Matchers}

class CartTest extends FunSpec with Matchers {
  describe("A Cart") {
    describe("when empty") {
      it("should have size 0") {
        new BasicCart() should have size 0
      }

      it("should have total cost of 0") {
        new BasicCart().totalCost shouldBe 0
      }

      it("should add an item") {
        val cart: Cart = new BasicCart()
        cart add Item(Product("banana"), ItemDetails(2, Price(0.50)))
        cart should have size 1
      }
    }
    describe("when not empty") {
      val items: Map[Product, ItemDetails] = Map(
        (Product("banana"), ItemDetails(3, Price(2))),
        (Product("apple"), ItemDetails(4, Price(1))),
        (Product("pineapple"), ItemDetails(2, Price(1))),
        (Product("milk"), ItemDetails(1, Price(10))),
      )
      val cart: Cart = new BasicCart(items)

      it("shouldn't have size 0") {
         cart should have size items.size
      }

      it("shouldn't have total cost of 0") {
        cart.totalCost shouldBe items.map(_._2.price.value).sum
      }

      it("should add an existing item") {
        cart add Item(Product("banana"), ItemDetails(4, Price(1.50)))
        cart.size shouldBe 4
        cart.content should contain (Item(Product("banana"), ItemDetails(7, Price(3.50))))
      }
    }
  }






}
